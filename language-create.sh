#!/bin/bash

PATH_TO_SOURCE="./"
PATH_TO_SOURCE="$(readlink -f "$PATH_TO_SOURCE")"

PATH_TO_LOCALE_LIST="${PATH_TO_SOURCE}/language.codes"
CONTROL_FILE="${PATH_TO_SOURCE}/debian/control"

add_package_control(){
    local LANGUAGE_CODE="${1}"
    local LANGUAGE_NAME="${2}"
    
    local LANG_CODE_MOD="$(echo "$LANGUAGE_CODE" | sed 's/_/-/' | tr "[A-Z]" "[a-z]")"
    
    cat >> "$CONTROL_FILE" << PACKAGE_TEMPLATE

Package: antix-base-l10n-${LANG_CODE_MOD}
Priority: optional
Section: localization
Architecture: all 
Depends: gettext
Conflicts: antix-base-l10n-all
Breaks: antix-base-l10n-all
Recommends: antix-goodies, locale-antix
Description: ${LANGUAGE_NAME} Language support for antiX programs
 This package contains the localization of antiX base programs in
 ${LANGUAGE_NAME} (${LANGUAGE_CODE}).
 .
 Collaborate with translations in 
 https://www.transifex.com/anticapitalista/antix-development
PACKAGE_TEMPLATE

}

add_package_install(){
    local LANGUAGE_CODE="${1}"
    local LANGUAGE_NAME="${2}"
    
    local LANG_CODE_MOD="$(echo "$LANGUAGE_CODE" | sed 's/_/-/' | tr "[A-Z]" "[a-z]")"
    local PACKAGE_NAME="antix-base-l10n-${LANG_CODE_MOD}"
    local INSTALL_PATH="${PATH_TO_SOURCE}/debian/${PACKAGE_NAME}.install"
    
    cat > "$INSTALL_PATH" << INSTALL_TEMPLATE
locale/${LANGUAGE_CODE}            usr/share/locale
INSTALL_TEMPLATE
}

# Generate project package info
while read -r line; do
	LANGUAGE_CODE="$(echo $line | cut -d"," -f1)"
    LANGUAGE_NAME="$(echo $line | cut -d" " -f3-)"
    
	if [ ! -z "$LANGUAGE_CODE" ] && [ ! -z "$LANGUAGE_NAME" ]; then
        echo "creating package for $LANGUAGE_NAME (${LANGUAGE_CODE})"
        add_package_control "$LANGUAGE_CODE" "$LANGUAGE_NAME"
        add_package_install "$LANGUAGE_CODE" "$LANGUAGE_NAME"
    fi
	
done < <(cat "$PATH_TO_LOCALE_LIST" | grep -v "^#")
