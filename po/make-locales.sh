#!/bin/bash

main(){
    SCRIPT_NAME="${1}"
    PO_FOLDER="${2}"
    
    PO_FOLDER="$(readlink -f "$PO_FOLDER")"
    
    if [ -z "$SCRIPT_NAME" ] || [ -z "$PO_FOLDER" ]; then exit 1; fi
    
    LOCALE_FOLDER="$(dirname "$PO_FOLDER")/locale"
    
    # Before starting, remove the old locale folder
    clean_locale
    
    # Process .po files for different folders
    if $(ls -d "${PO_FOLDER}"/*/ 1>/dev/null 2>/dev/null); then
        for folder in "${PO_FOLDER}"/*/; do
            FILE_NAME="$(echo "$folder" | rev | cut -d"/" -f2 | rev)"
            create_locales "${PO_FOLDER}/$FILE_NAME" "$LOCALE_FOLDER" "$FILE_NAME"
        done
    fi
    
    # If folder contains .po files, process them
    if $(ls "${PO_FOLDER}"/*.po 1>/dev/null 2>/dev/null); then
        create_locales "${PO_FOLDER}" "$LOCALE_FOLDER" "$SCRIPT_NAME"
    fi
}

clean_locale(){
    if [ -d "$LOCALE_FOLDER" ]; then
        rm -r "$LOCALE_FOLDER"
    fi
}

create_locales(){
    local FILE_NAME FOLDER_IN FOLDER_OUT PESKY_NAME MO_NAME MO_FOLDER
    FOLDER_IN="${1}"
    FOLDER_OUT="${2}"
    FILE_NAME="${3}"
    
    MO_NAME="$FILE_NAME"
    
    # if the .po files also contain a name before the language code
    PESKY_NAME="$(pesky_name "$FOLDER_IN")"
    
    for file in "${FOLDER_IN}"/*.po ; do
        LANGUAGE_CODE="$(echo "${file##*/}" | cut -d"." -f1)"
        if [ ! -z "$PESKY_NAME" ]; then
            LANGUAGE_CODE="$(echo "$LANGUAGE_CODE" | sed "s/${PESKY_NAME}_//")"
        fi
        
        # Make language locale folder
        MO_FOLDER="${FOLDER_OUT}/${LANGUAGE_CODE}/LC_MESSAGES"
        if [ ! -d "$MO_FOLDER" ]; then
            mkdir -p "$MO_FOLDER"
        fi
        # Convert .po to .mo
        msgfmt "$file" -o "${MO_FOLDER}/${MO_NAME}.mo"
    done
}

pesky_name(){
    local CHECK_FOLDER TOTAL_PO STRIP_NAME TOTAL_STRIP
    CHECK_FOLDER="${1}"
    
    TOTAL_PO=$(ls -1 "$CHECK_FOLDER"/*.po | wc -l | awk '{ print $1 }')
    
    STRIP_NAME="$(ls -1 "$CHECK_FOLDER"/*.po | rev | cut -d"/" -f1 | rev | cut -d"_" -f1 | sort -u)"
    
    TOTAL_STRIP=$(echo "$STRIP_NAME" | wc -l | awk '{ print $1 }')
    
    # If less than 3 .po files or no pesky name found, return
    if [ $TOTAL_PO -lt 3 ] || [ $TOTAL_STRIP -gt 1 ]; then return 1; fi
    
    # This is the pesky name
    echo "$STRIP_NAME"
}

main "$@"
