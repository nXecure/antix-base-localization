# antix-base-localization

Source containing translation for base antiX apps

## First time setting up the source package

1. Select what language packages you want to create in the _language.codes_ file. Comment out any language you don't want to include in a separate package.
2. All programs/scripts with translations should have their own separate folder inside the **po** folder, which will contain the .po files for all languages (folder name should match translation .po file name). The current po folder is empty, to be filled by the maintainer (a suggestion of possible scripts is contained inside the file _po-names_, as reference).
3. Run the script **_language-create.sh_** to create the install instructions and the debian/control information for each language package (based on the ones in the _language.codes_ file)
4. At build time, the locale folder will be created (based on debian/rules instructions), and the control and install istructions will create the packages for each language.

**Note:** By default, a package named "antix-base-l10n-all" is defined in debian/control, and this package will contain all translations generated in this source. This package will conflict with individual language packages created from this source, so that it will be removed when they are installed.
